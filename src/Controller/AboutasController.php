<?php

namespace App\Controller;

use App\Repository\AboutasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/aboutas")
 */

class AboutasController extends AbstractController
{
	/**
	 * @var AboutasRepository
	 */
	private $aboutasRepository;
	
	/**
	 * @param AboutasRepository $aboutasRepository
	 */
	public function __construct(AboutasRepository $aboutasRepository)
	{
		$this->aboutasRepository = $aboutasRepository;
	}
	
	/**
     * @Route("/", name="aboutas")
     */
    public function index(): Response
    {
        return $this->render('aboutas/index.html.twig', [
            'aboutas' => $this->aboutasRepository->findAllAboutAs()
        ]);
    }
}
