<?php

namespace App\Controller;

use App\Repository\PriceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/piece")
 */
class PieceController extends AbstractController
{
	/**
	 * @var PriceRepository
	 */
	private $priceRepository;
	
	/**
	 * @param PriceRepository $priceRepository
	 */
	public function __construct(PriceRepository $priceRepository)
	{
		$this->priceRepository = $priceRepository;
	}
	
	/**
     * @Route("/", name="piece")
     */
    public function index(): Response
    {
        return $this->render('piece/index.html.twig', [
            'price' => $this->priceRepository->findAllPrice()
        ]);
    }
}
