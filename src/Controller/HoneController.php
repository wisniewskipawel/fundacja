<?php

namespace App\Controller;

use App\Form\SearchInputType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class HoneController extends AbstractController
{
	/**
     * @Route("/", name="hone")
     */
    public function index(): Response
    {
        return $this->render('hone/index.html.twig', [
            'controller_name' => 'HoneController',
        ]);
    }
}
