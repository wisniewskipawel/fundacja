<?php

namespace App\Controller;

use App\Repository\AuctionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/auction")
 */

class AuctionController extends AbstractController
{
	/**
	 * @var AuctionRepository
	 */
	private $auctionRepository;
	
	/**
	 * @param AuctionRepository $auctionRepository
	 */
	public function __construct(AuctionRepository $auctionRepository)
	{
		$this->auctionRepository = $auctionRepository;
	}
	
	/**
     * @Route("/", name="auction")
     */
    public function index(): Response
    {
        return $this->render('auction/index.html.twig', [
            'auction' => $this->auctionRepository->findAllAuction()
        ]);
    }
}
