<?php

namespace App\Controller;

use App\Repository\AutographsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/autographs")
 */

class AutographsController extends AbstractController
{
	/**
	 * @var AutographsRepository
	 */
	private $autographsRepository;
	
	/**
	 * @param AutographsRepository $autographsRepository
	 */
	public function __construct(AutographsRepository $autographsRepository)
	{
		$this->autographsRepository = $autographsRepository;
	}
	
	/**
     * @Route("/", name="autographs")
     */
    public function index(): Response
    {
        return $this->render('autographs/index.html.twig', [
            'autographs' => $this->autographsRepository->findAllAutographs()
        ]);
    }
}
