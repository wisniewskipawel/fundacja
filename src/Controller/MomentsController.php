<?php

namespace App\Controller;

use App\Repository\MomentsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/moments")
 */

class MomentsController extends AbstractController
{
	/**
	 * @var MomentsRepository
	 */
	private $momentsRepository;
	
	/**
	 * @param MomentsRepository $momentsRepository
	 */
	public function __construct(MomentsRepository $momentsRepository)
	{
		$this->momentsRepository = $momentsRepository;
	}
	
	/**
     * @Route("/", name="moments")
     */
    public function index(): Response
    {
        return $this->render('moments/index.html.twig', [
            'moments' => $this->momentsRepository->findAllMoments()
        ]);
    }
}
