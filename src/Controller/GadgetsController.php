<?php

namespace App\Controller;

use App\Repository\GadgetsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gadgets")
 */

class GadgetsController extends AbstractController
{
	private $gadgetsRepository;
	
	public function __construct(GadgetsRepository $gadgetsRepository)
	{
		$this->gadgetsRepository = $gadgetsRepository;
	}
	
	/**
     * @Route("/", name="gadgets")
     */
    public function index(): Response
    {

        return $this->render('gadgets/index.html.twig', [
            'gadgets' => $this->gadgetsRepository->findAllGadgets()
        ]);
    }
}
