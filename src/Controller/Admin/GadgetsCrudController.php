<?php

namespace App\Controller\Admin;

use App\Entity\Gadgets;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;

class GadgetsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Gadgets::class;
    }

 
    public function configureFields(string $pageName): iterable
    {
        return [
    //        IdField::new('id'),
            TextField::new('name'),
            TextEditorField::new('description'),
	        ImageField::new('photo')
	                ->setBasePath('img/')
	                ->setUploadDir('public/img')
	                ,
	        //  FileUploadType::c('photo'),
        ];
    }
   
}
