<?php

namespace App\Controller\Admin;

use App\Entity\Aboutas;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AboutasCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Aboutas::class;
    }
	
    public function configureFields(string $pageName): iterable
    {
		return [
		    TextField::new('firstName'),
		    TextField::new('lastName'),
	        TextEditorField::new('description'),
	        ImageField::new('photo')
		        ->setBasePath('img/photo')
		        ->setUploadDir('public/img/photo'),
		];
    }
}
