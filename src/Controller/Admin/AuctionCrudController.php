<?php

namespace App\Controller\Admin;

use App\Entity\Auction;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AuctionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Auction::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
	        TextField::new('url'),
	        NumberField::new('price'),
	        TextEditorField::new('description'),
	        DateTimeField::new('createAd'),
	        ImageField::new('photo')
		        ->setBasePath('img/auction')
		        ->setUploadDir('public/img/auction'),
        ];
    }
}
