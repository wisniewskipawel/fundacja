<?php

namespace App\Controller\Admin;

use App\Entity\Aboutas;
use App\Entity\Auction;
use App\Entity\Autographs;
use App\Entity\Contact;
use App\Entity\Moments;
use App\Entity\Price;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Gadgets;

/**
 * @Route("/admin")
 */

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
	    $routeBuilder = $this->get(AdminUrlGenerator::class);
		
	    return $this->redirect($routeBuilder->setController(GadgetsCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Fundacja');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
		yield MenuItem::linkToCrud('The Price', 'fas fa-list', Price::class);
	    yield MenuItem::linkToCrud('The Moments', 'fas fa-list', Moments::class);
	    yield MenuItem::linkToCrud('The About As', 'fas fa-list', Aboutas::class);
	    yield MenuItem::linkToCrud('The Auction', 'fas fa-list', Auction::class);
	    yield MenuItem::linkToCrud('The Autographs', 'fas fa-list', Autographs::class);
	    yield MenuItem::linkToCrud('The Contact', 'fas fa-list', Contact::class);
    }
}
