<?php

namespace App\Controller\Admin;

use App\Entity\Autographs;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AutographsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Autographs::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextEditorField::new('description'),
	        ImageField::new('photo')
		        ->setBasePath('img')
		        ->setUploadDir('public/img'),

        ];
    }
}
