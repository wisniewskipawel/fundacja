<?php

namespace App\Controller\Admin;

use App\Entity\Price;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class PriceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Price::class;
    }
	
    public function configureFields(string $pageName): iterable
    {
        return [
	        ImageField::new('photo')
		        ->setBasePath('img/')
		        ->setUploadDir('public/img'),
            NumberField::new('price'),
	        TextEditorField::new('description')
        ];
    }
}
