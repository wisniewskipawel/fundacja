<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\SendMailType;
use App\Repository\ContactRepository;
use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;

/**
 * @Route("/contact")
 */

class ContactController extends AbstractController
{
	
	private $contactRepository;
	
	public function __construct(ContactRepository $contactRepository)
	{
		$this->contactRepository = $contactRepository;
	}
	
	/**
     * @Route("/", name="contact")
     */
    public function index(Request $request, MailService $mailService): Response
    {
		$contact = new Contact();
		$form = $this->createForm(SendMailType::class,$contact);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->contactRepository->saveMessage($contact);
			$mailService->sendMail($form);
			
			return $this->redirectToRoute('hone');
		}
		return $this->renderForm('contact/index.html.twig',[
			'form' => $form,
			'controller_name' => 'controller_name'
		]);
    }
}