<?php

namespace App\Repository;

use App\Entity\Aboutas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Aboutas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aboutas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aboutas[]    findAll()
 * @method Aboutas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AboutasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aboutas::class);
    }
	
	public function findAllAboutAs()
	{
		return $this->createQueryBuilder('a')
			->orderBy('a.id', 'ASC')
			->getQuery()
			->getResult();
	}

    // /**
    //  * @return Aboutas[] Returns an array of Aboutas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Aboutas
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
