<?php

namespace App\Repository;

use App\Entity\Autographs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Autographs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Autographs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Autographs[]    findAll()
 * @method Autographs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutographsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Autographs::class);
    }
	
	public function findAllAutographs()
	{
		return $this->createQueryBuilder('a')
			->orderBy('a.id', 'ASC')
			->getQuery()
			->getResult()
			;
	}

    // /**
    //  * @return Autographs[] Returns an array of Autographs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Autographs
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
