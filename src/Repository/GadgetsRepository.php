<?php

namespace App\Repository;

use App\Entity\Gadgets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gadgets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gadgets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gadgets[]    findAll()
 * @method Gadgets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GadgetsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gadgets::class);
    }
	
	public function findAllGadgets()
	{
		return $this->createQueryBuilder('g')
			->orderBy('g.id', 'ASC')
			->getQuery()
			->getResult();
	}

    // /**
    //  * @return Gadgets[] Returns an array of Gadgets objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gadgets
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
