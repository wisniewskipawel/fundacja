<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailService {
	
	/**
	 * @var MailerInterface
	 */
	
	private $mailer;
	
	/**
	 * @param MailerInterface $mailer
	 */
	
	public function __construct(MailerInterface $mailer)
	{
		$this->mailer = $mailer;
	}
	
	public function sendMail($data)
	{
		$email = (new Email())
			->from($data->get('adress')->getData())
			->to('caton1982@gmail.com')
			->subject('Contakt')
			->text($data->get('message')->getData());
		$this->mailer->send($email);

	}
}